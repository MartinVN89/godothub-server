"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const component_emitter_1 = __importDefault(require("component-emitter"));
const ws_1 = __importDefault(require("ws"));
function default_1(sock) {
    const listeners = new component_emitter_1.default();
    // tslint:disable-next-line:ban-types
    let onopenHandlers = [];
    function onmessage(event) {
        let json;
        let args;
        try {
            json = JSON.parse(event.data);
            // args = [json.event].concat(json.data);
            args = [json.event, json.data];
        }
        catch (e) {
            onerror(e);
            return;
        }
        listeners.emit.apply(listeners, args);
    }
    function onerror(err) {
        listeners.emit('error');
    }
    function onclose(event) {
        listeners.emit.apply(listeners, ['close', event]);
    }
    function onopen() {
        onopenHandlers.forEach((fn) => {
            fn();
        });
        onopenHandlers = [];
    }
    // tslint:disable-next-line:ban-types
    function whenOpen(fn) {
        if (sock.readyState === ws_1.default.OPEN) {
            fn();
        }
        else {
            onopenHandlers.push(fn);
        }
    }
    sock.onmessage = onmessage;
    sock.onerror = onerror;
    sock.onopen = onopen;
    sock.onclose = onclose;
    function emit(event, data) {
        whenOpen(() => {
            sock.send(JSON.stringify({ event, data }));
        });
        return events;
    }
    // tslint:disable-next-line:ban-types
    function on(type, cb) {
        listeners.on(type, cb);
        return events;
    }
    // tslint:disable-next-line:ban-types
    function off(type, cb) {
        listeners.off(type, cb);
        return events;
    }
    const events = Object.create(sock);
    events.socket = sock;
    events.emit = emit;
    events.on = on;
    events.off = off;
    events.listeners = listeners.listeners.bind(listeners);
    events.hasListeners = listeners.hasListeners.bind(listeners);
    return events;
}
exports.default = default_1;
//# sourceMappingURL=ws-events.js.map