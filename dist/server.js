"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const ws_1 = __importDefault(require("ws"));
const GameLobby_1 = require("./GameLobby");
const ws_events_1 = __importDefault(require("./ws-events"));
const WebSocketServer = ws_1.default.Server;
const app = express_1.default();
const port = process.env.PORT || 5000;
app.use(express_1.default.static(__dirname + '/'));
const server = http_1.default.createServer(app);
server.listen(port);
console.log('http server listening on %d', port);
const wss = new WebSocketServer({ server });
console.log('websocket server created');
const Games = [];
const CLIENTS = [];
// tslint:disable-next-line:no-empty
function noop() { }
function heartbeat() {
    this.isAlive = true;
}
// new connection etablished
wss.on('connection', (ws, req) => {
    console.log('new connection');
    sendAll('New client connected');
    const events = ws_events_1.default(ws);
    ws.isAlive = true;
    ws.on('pong', heartbeat);
    const IP = get_ip(req);
    CLIENTS.push(ws);
    events.on('message', (data) => { broadcast(ws, 'message', data.message); });
    events.on('getGameLobbyList', () => {
        const filteredGames = Games.map((game) => { const { hostSocket } = game, g = __rest(game, ["hostSocket"]); return g; });
        console.log(filteredGames);
        sendEvent(ws, 'getGameLobbyList', filteredGames);
    });
    events.on('createGameLobby', (data) => {
        if (!GameLobby_1.isDataValid(data)) {
            sendMessage(ws, 'Failed to add GameLobby. invalid request');
            return;
        }
        if (gameHostIndexOf(ws) === -1) {
            data.hostSocket = ws;
            data.IP = IP;
            Games.push(data);
            sendMessage(ws, 'Successfully added GameLobby to the list');
        }
        else {
            sendMessage(ws, 'Failed to add GameLobby. a host with the same connection already added');
        }
    });
    events.on('updateGameLobby', (data) => {
        if (!GameLobby_1.isDataValid(data)) {
            sendMessage(ws, 'Failed to update GameLobby. invalid request');
            return;
        }
        const gamehostIndex = gameHostIndexOf(ws);
        if (gamehostIndex !== -1) {
            let gameLobby = Games[gamehostIndex];
            gameLobby = Object.assign({}, gameLobby, data);
            Games[gamehostIndex] = gameLobby;
            sendMessage(ws, 'GameLobby updated');
        }
        else {
            sendMessage(ws, 'Failed to update GameLobby. Could not find the old one to update');
        }
    });
    events.on('close', (data) => {
        console.log('connection closed');
        CLIENTS.splice(CLIENTS.indexOf(ws), 1);
        const gamehostIndex = gameHostIndexOf(ws);
        if (gamehostIndex !== -1) {
            Games.splice(gamehostIndex, 1);
            console.log('Removed GameLobby for closed connection');
        }
    });
});
const interval = setInterval(function ping() {
    CLIENTS.forEach(function each(ws) {
        if (ws.isAlive === false) {
            return ws.terminate();
        }
        ws.isAlive = false;
        ws.ping(noop);
    });
}, 30000);
function get_ip(req) {
    let ipProxy = req.headers['x-forwarded-for'];
    let ip;
    if (ipProxy instanceof Array) {
        ipProxy = ipProxy.join(' ');
    }
    if (ipProxy) {
        const list = ipProxy.split(',');
        ip = list[list.length - 1];
    }
    else {
        ip = req.connection.remoteAddress;
    }
    return ip;
}
function broadcast(sender, event, data) {
    CLIENTS.forEach((client) => {
        if (client !== sender) {
            sendEvent(client, event, data);
        }
    });
}
function sendAll(message) {
    CLIENTS.forEach((client) => { sendMessage(client, message); });
}
function sendMessage(client, message) {
    sendEvent(client, 'message', { message });
}
function sendEvent(client, event, data) {
    client.send(JSON.stringify({ event, data }));
}
function gameHostIndexOf(ws) {
    return Games.findIndex((gameLobby) => gameLobby.hostSocket === ws);
}
//# sourceMappingURL=server.js.map