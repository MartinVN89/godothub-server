"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ajv_1 = __importDefault(require("ajv"));
const schemas_json_1 = __importDefault(require("./json_schemas/schemas.json"));
const ajv = new ajv_1.default();
const gameLobbyDataValidator = ajv.compile(schemas_json_1.default.definitions.GameLobbyData);
function isDataValid(data) {
    if (gameLobbyDataValidator(data)) {
        return true;
    }
    else {
        console.log(gameLobbyDataValidator.errors);
        return false;
    }
}
exports.isDataValid = isDataValid;
//# sourceMappingURL=GameLobby.js.map