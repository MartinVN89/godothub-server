import Emitter from 'component-emitter';
import WebSocket from 'ws';
import {Message} from './json_schemas/json_types';

export default function(sock: WebSocket) {
  const listeners = new Emitter();
  // tslint:disable-next-line:ban-types
  let onopenHandlers: Function[] = [];

  function onmessage(event: { data: string; type: string; target: WebSocket }) {
    let json: Message;
    let args;
    try {
      json = JSON.parse(event.data);
      // args = [json.event].concat(json.data);
      args = [json.event , json.data];
    } catch (e) {
      onerror(e);
      return;
    }
    listeners.emit.apply(listeners, args);
  }

  function onerror(err: any) {
    listeners.emit('error');
  }

  function onclose(event: { wasClean: boolean; code: number; reason: string; target: WebSocket }) {
    listeners.emit.apply(listeners, ['close', event]);
  }

  function onopen() {
    onopenHandlers.forEach((fn) => {
      fn();
    });
    onopenHandlers = [];
  }

  // tslint:disable-next-line:ban-types
  function whenOpen(fn: Function) {
    if (sock.readyState === WebSocket.OPEN) {
      fn();
    } else {
      onopenHandlers.push(fn);
    }
  }

  sock.onmessage = onmessage;
  sock.onerror = onerror;
  sock.onopen = onopen;
  sock.onclose = onclose;

  function emit(event: string, data: any) {
    whenOpen(() => {
      sock.send(JSON.stringify({ event, data }));
    });
    return events;
  }

  // tslint:disable-next-line:ban-types
  function on(type: string, cb: Function) {
    listeners.on(type, cb);
    return events;
  }

  // tslint:disable-next-line:ban-types
  function off(type: string, cb: Function) {
    listeners.off(type, cb);
    return events;
  }

  const events = Object.create(sock);
  events.socket = sock;
  events.emit = emit;
  events.on = on;
  events.off = off;
  events.listeners = listeners.listeners.bind(listeners);
  events.hasListeners = listeners.hasListeners.bind(listeners);

  return events;
}
