export interface GameLobbyData {
    localIP: string;
    port: number;
    gameName: string;
    gameID: string;
    hostName: string;
    currentPlayers: number;
    maxPlayers: number;
}

export interface Message {
    event: string;
    data: any;
}
