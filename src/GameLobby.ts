import AJV from 'ajv';
import { GameLobbyData } from './json_schemas/json_types';
import schema from './json_schemas/schemas.json';
import { mySocket } from './server';

export interface GameLobby extends GameLobbyData {
  hostSocket: mySocket;
  IP: string;
}

const ajv = new AJV();
const gameLobbyDataValidator = ajv.compile(schema.definitions.GameLobbyData);

export function isDataValid(data: any): boolean {
    if (gameLobbyDataValidator(data)) {
        return true;
    } else {
        console.log(gameLobbyDataValidator.errors);
        return false;
    }
}
