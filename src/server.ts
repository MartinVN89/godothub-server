import express from 'express';
import http from 'http';
import WebSocket from 'ws';
import { GameLobby, isDataValid } from './GameLobby';
import wsEvents from './ws-events';
export type mySocket = WebSocket & { isAlive: boolean };
const WebSocketServer = WebSocket.Server;

const app = express();
const port = process.env.PORT || 5000;

app.use(express.static(__dirname + '/'));

const server = http.createServer(app);
server.listen(port);
console.log('http server listening on %d', port);
const wss = new WebSocketServer({ server });
console.log('websocket server created');

const Games: GameLobby[] = [];
const CLIENTS: mySocket[] = [];

// tslint:disable-next-line:no-empty
function noop() { }

function heartbeat() {
  this.isAlive = true;
}

// new connection etablished
wss.on('connection', (ws: mySocket, req) => {
  console.log('new connection');
  sendAll('New client connected');
  const events = wsEvents(ws);
  ws.isAlive = true;
  ws.on('pong', heartbeat);

  const IP: string = get_ip(req);

  CLIENTS.push(ws);
  events.on('message', (data: any) => { broadcast(ws, 'message', data.message); });
  events.on('getGameLobbyList', () => {
    const filteredGames = Games.map((game) => { const { hostSocket, ...g} = game; return g; })
    console.log(filteredGames);
    sendEvent(ws, 'getGameLobbyList', filteredGames); 
  });
  events.on('createGameLobby', (data: any) => {
    if (!isDataValid(data)) {
      sendMessage(ws, 'Failed to add GameLobby. invalid request');
      return;
    }
    if (gameHostIndexOf(ws) === -1) {
      data.hostSocket = ws;
      data.IP = IP;
      Games.push(data);
      sendMessage(ws, 'Successfully added GameLobby to the list');
    } else {
      sendMessage(ws, 'Failed to add GameLobby. a host with the same connection already added');
    }
  });
  events.on('updateGameLobby', (data: any) => {
    if (!isDataValid(data)) {
      sendMessage(ws, 'Failed to update GameLobby. invalid request');
      return;
    }
    const gamehostIndex = gameHostIndexOf(ws);
    if (gamehostIndex !== -1) {
      let gameLobby = Games[gamehostIndex];
      gameLobby = { ...gameLobby, ...data };
      Games[gamehostIndex] = gameLobby;
      sendMessage(ws, 'GameLobby updated');
    } else {
      sendMessage(ws, 'Failed to update GameLobby. Could not find the old one to update');
    }
  });
  events.on('close', (data: any) => {
    console.log('connection closed');
    CLIENTS.splice(CLIENTS.indexOf(ws), 1);
    const gamehostIndex = gameHostIndexOf(ws);
    if (gamehostIndex !== -1) {
      Games.splice(gamehostIndex, 1);
      console.log('Removed GameLobby for closed connection');
    }
  });
});

const interval = setInterval(function ping() {
  CLIENTS.forEach(function each(ws: mySocket) {
    if (ws.isAlive === false) { return ws.terminate(); }

    ws.isAlive = false;
    ws.ping(noop);
  });
}, 30000);

function get_ip(req: http.IncomingMessage) {
  let ipProxy = req.headers['x-forwarded-for'];
  let ip: string;
  if (ipProxy instanceof Array) {
    ipProxy = ipProxy.join(' ');
  }
  if (ipProxy) {
    const list = ipProxy.split(',');
    ip = list[list.length - 1];
  } else {
    ip = req.connection.remoteAddress;
  }
  return ip;
}

function broadcast(sender: WebSocket, event: string, data: any) {
  CLIENTS.forEach((client) => {
    if (client !== sender) {
      sendEvent(client, event, data);
    }
  });
}

function sendAll(message: string) {
  CLIENTS.forEach((client) => { sendMessage(client, message); });
}

function sendMessage(client: WebSocket, message: string) {
  sendEvent(client, 'message', { message });
}

function sendEvent(client: WebSocket, event: string, data: any) {
  client.send(JSON.stringify({ event, data }));
}

function gameHostIndexOf(ws: mySocket) {
  return Games.findIndex((gameLobby: GameLobby) =>  gameLobby.hostSocket === ws);
}
